# Videos

<h3> Installation </h3>

Download this module and add in module directory and enable it.

<h3> How to work? </h3>
This module create a vocabulary that named <code>videos</code>.<br>

<b>Fields:</b>
 <br><code> field_video_enable </code>
 <br><code> field_show_slides </code>
 <br><code> field_video_file </code>

<b>Route request:</b>
 <br><code> api/videos </code> 

<b>Response:</b>
 <br><code>name</code>  Example: Lorem ipsum
 <br><code>show_slides</code>  Example: True or False 
 <br><code>src</code> Example: Source of video
 
 
