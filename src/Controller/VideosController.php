<?php

namespace Drupal\videos\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 1/31/17
 * Time: 11:11 AM
 */
class VideosController {

    public function index() {

        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
            'vid'                => 'videos',
            'field_video_enable' => true,
        ]);

        usort($terms, function ($a, $b) {
            if ($a->getWeight() == $b->getWeight()) {
                return 0;
            }

            return ($a->getWeight() < $b->getWeight()) ? -1 : 1;
        });

        $result = array();
        foreach ($terms as $term) {

            $result[] = array(
                'name'        => $term->getName(),
                'show_slides' => $term->get('field_show_slides')->getValue()[0]['value'],
                'src'         => file_create_url(\Drupal\file\Entity\File::load($term->get('field_video_file')->getValue()[0]['target_id'])->getFileUri()),
            );
        }

        return new JsonResponse($result);
    }

}